package array

import (
	"sort"
)

// Window describe structure of sliding window implementation based on simple array
type Window struct {
	cursor int
	size   int

	items []int
}

// New creates new sliding window implementation based on simple array
func New(size int) *Window {
	return &Window{
		cursor: 0,
		size:   size,
		items:  make([]int, 0, size),
	}
}

// AddDelay adds new delay into window
func (w *Window) AddDelay(delay int) {
	if w.cursor >= len(w.items) {
		w.items = append(w.items, delay)
	} else {
		w.items[w.cursor] = delay
	}

	if w.cursor+1 == w.size {
		w.cursor = 0
	} else {
		w.cursor++
	}
}

// GetMedian returns window of delays in window
func (w *Window) GetMedian() float64 {
	length := len(w.items)
	if length <= 1 {
		return -1
	}

	sorted := append([]int(nil), w.items...)
	sort.Ints(sorted)

	var median float64
	if length%2 == 0 {
		median = float64(sorted[length/2-1]+sorted[length/2]) / 2
	} else {
		median = float64(sorted[length/2])
	}

	return median
}
