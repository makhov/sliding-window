// Package provides an naive window.MedianInterface implementation based on array, which
// gets sorted every time when median counts.
// Of course it slow, because it costs O(n log n).

package array
