// Package provides an window.MedianInterface implementation based on two arrays, one which
// store data points order, other always is sorted. Searching for deleted element and place
// for inserted carries out with binary search.
// Much faster then "array" implementation. O(log n).

package binary
