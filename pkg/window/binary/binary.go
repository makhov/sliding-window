package binary

import (
	"sort"
)

// Window describe structure of sliding window implementation based on array
type Window struct {
	cursor int
	size   int

	items  []int
	sorted []int
}

// New creates new sliding window implementation based on array
func New(size int) *Window {
	return &Window{
		cursor: 0,
		size:   size,
		items:  make([]int, 0, size),
		sorted: make([]int, 0, size+1), // size+1 because we need one extra element and want to avoid reallocation
	}
}

// AddDelay adds new delay into window
func (w *Window) AddDelay(delay int) {
	var prev int
	if w.cursor >= len(w.items) {
		w.items = append(w.items, delay)
	} else {
		prev = w.items[w.cursor]
		w.items[w.cursor] = delay
	}

	delPos := -1
	insPos := sort.Search(len(w.sorted), func(i int) bool {
		return w.sorted[i] >= delay
	})
	if prev != 0 {
		delPos = sort.Search(len(w.sorted), func(i int) bool {
			return w.sorted[i] >= prev
		})
	}

	w.sorted = insert(w.sorted, insPos, delay)
	if delPos != -1 {
		if insPos <= delPos {
			delPos += 1
		}
		w.sorted = delete(w.sorted, delPos)
	}

	if w.cursor+1 == w.size {
		w.cursor = 0
	} else {
		w.cursor++
	}
}

// GetMedian returns window of delays in window
func (w *Window) GetMedian() float64 {
	length := len(w.items)
	if length <= 1 {
		return -1
	}

	//sorted := append([]int(nil), w.items...)
	//sort.Ints(sorted)

	var median float64
	if length%2 == 0 {
		median = float64(w.sorted[length/2-1]+w.sorted[length/2]) / 2
	} else {
		median = float64(w.sorted[length/2])
	}

	return median
}

func insert(arr []int, i, val int) []int {
	return append(arr[:i], append([]int{val}, arr[i:]...)...)
}

func delete(arr []int, i int) []int {
	return append(arr[:i], arr[i+1:]...)
}

func (w *Window) Sorted() []int {
	sorted := append([]int(nil), w.items...)
	sort.Ints(sorted)
	return w.sorted
}
