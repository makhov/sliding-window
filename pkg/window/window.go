package window

import (
	"errors"

	"github.com/makhov/sliding-window/pkg/window/array"
	"github.com/makhov/sliding-window/pkg/window/binary"
)

// MedianInterface counts median
type MedianInterface interface {
	AddDelay(delay int)
	GetMedian() float64
}

// New creates new MedianInterface implementation
func New(alg string, size int) (MedianInterface, error) {
	switch alg {
	case "array":
		return array.New(size), nil
	case "binary":
		return binary.New(size), nil
	default:
		return nil, errors.New("unknown implementation type")
	}
}
