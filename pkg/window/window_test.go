package window

import (
	"math/rand"
	"testing"

	"github.com/makhov/sliding-window/pkg/window/array"
	"github.com/makhov/sliding-window/pkg/window/binary"
)

func TestSimple_Array(t *testing.T) {
	var testCases = []struct {
		Size      int
		Delays    []int
		ExpMedian []float64
	}{
		{
			Size:      3,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101, 102, 110, 115},
		},
		{
			Size:      2,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101.5, 105.5, 115, 117.5},
		},
		{
			Size:      5,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101, 101.5, 102, 110},
		},
	}

	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			m := array.New(tc.Size)

			for i, d := range tc.Delays {
				m.AddDelay(d)
				actual := m.GetMedian()
				expected := tc.ExpMedian[i]

				if actual != expected {
					t.Errorf("Unexpected median. Expected: %.1f, actual: %.1f", expected, actual)
				}
			}
		})
	}
}

func TestWindow_Bubble_Simple(t *testing.T) {
	var testCases = []struct {
		Size      int
		Delays    []int
		ExpMedian []float64
	}{
		{
			Size:      3,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101, 102, 110, 115},
		},
		{
			Size:      2,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101.5, 105.5, 115, 117.5},
		},
		{
			Size:      5,
			Delays:    []int{100, 102, 101, 110, 120, 115},
			ExpMedian: []float64{-1, 101, 101, 101.5, 102, 110},
		},
	}

	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			m := binary.New(tc.Size)

			for i, d := range tc.Delays {
				m.AddDelay(d)
				actual := m.GetMedian()
				expected := tc.ExpMedian[i]

				if actual != expected {
					t.Errorf("Unexpected median. Expected: %.1f, actual: %.1f", expected, actual)
				}
			}
		})
	}
}

func TestEquality(t *testing.T) {
	size := 100
	delays := make([]int, 1000)
	for i := 0; i < len(delays); i++ {
		r := rand.Intn(100)
		if r == 0 {
			r = 2
		}
		delays[i] = r
	}

	arr := array.New(size)
	bub := binary.New(size)
	for _, d := range delays {
		arr.AddDelay(d)
		bub.AddDelay(d)

		arrMed := arr.GetMedian()
		bubMed := bub.GetMedian()
		if arrMed != bubMed {
			t.Fatalf("Medians are not equals. array: %.1f, binary: %.1f", arrMed, bubMed)
		}
	}
}

func benchmarkAll(b *testing.B, w MedianInterface, size int, delays []int) {
	for n := 0; n < b.N; n++ {
		for _, d := range delays {
			w.AddDelay(d)
			w.GetMedian()
		}
	}
}

func BenchmarkAllSmall_Array(b *testing.B) {
	size := 3
	delays := []int{100, 102, 101, 110, 120, 115}

	w := array.New(size)
	benchmarkAll(b, w, size, delays)
}

func BenchmarkAllSmall_Bubble(b *testing.B) {
	size := 3
	delays := []int{100, 102, 101, 110, 120, 115}

	w := binary.New(size)
	benchmarkAll(b, w, size, delays)
}

func BenchmarkAllLarge_Array(b *testing.B) {
	size := 1000
	delays := make([]int, 10000)
	for i := 0; i < len(delays); i++ {
		delays[i] = rand.Intn(100)
	}

	w := array.New(size)
	benchmarkAll(b, w, size, delays)
}

func BenchmarkAllLarge_Bubble(b *testing.B) {
	size := 1000
	delays := make([]int, 10000)
	for i := 0; i < len(delays); i++ {
		delays[i] = rand.Intn(100)
	}

	w := binary.New(size)
	benchmarkAll(b, w, size, delays)
}

func BenchmarkOneLarge_Array(b *testing.B) {
	w := array.New(1000)
	for n := 0; n < b.N; n++ {
		d := rand.Intn(100)
		w.AddDelay(d)
		w.GetMedian()
	}
}

func BenchmarkOneLarge_Binary(b *testing.B) {
	w := binary.New(1000)
	for n := 0; n < b.N; n++ {
		d := rand.Intn(100)
		w.AddDelay(d)
		w.GetMedian()
	}
}

func BenchmarkOneHuge_Binary(b *testing.B) {
	w := binary.New(10000)
	for n := 0; n < b.N; n++ {
		d := rand.Intn(100)
		w.AddDelay(d)
		w.GetMedian()
	}
}
