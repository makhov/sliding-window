package app

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestRun_Success(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	var testCases = []struct {
		Name      string
		File      string
		Size      int
		Algorithm string
	}{
		{Name: "test2.csv - array", File: "test2.csv", Size: 100, Algorithm: "array"},
		{Name: "test3.csv - array", File: "test3.csv", Size: 1000, Algorithm: "array"},
		{Name: "test4.csv - array", File: "test4.csv", Size: 10000, Algorithm: "array"},
		{Name: "test2.csv - binary", File: "test2.csv", Size: 100, Algorithm: "binary"},
		{Name: "test3.csv - binary", File: "test3.csv", Size: 1000, Algorithm: "binary"},
		{Name: "test4.csv - binary", File: "test4.csv", Size: 10000, Algorithm: "binary"},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			var (
				dataFile = "testdata/" + tc.File
				outFile  = dataFile + ".out"
			)

			var buf bytes.Buffer
			err := Run(dataFile, tc.Algorithm, tc.Size, &buf)
			if err != nil {
				t.Fatalf("unexpected error in Run: %v", err)
			}

			b, err := ioutil.ReadFile(outFile)
			if err != nil {
				t.Fatalf("error reading file: %v", err)
			}

			if !bytes.Equal(buf.Bytes(), b) {
				t.Errorf("Unexpected outpun.\nActual: %s\nExpected: content of %s", buf.String(), outFile)
			}
		})
	}
}
