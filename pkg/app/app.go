package app

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/makhov/sliding-window/pkg/window"
)

// Run starts program execution: reads data points from file, calculates median and write result to output
func Run(filepath, alg string, size int, w io.Writer) error {
	var (
		delays  = make(chan int, size)
		medians = make(chan float64, 1000)
		errc    = make(chan error)
	)

	wind, err := window.New(alg, size)
	if err != nil {
		return fmt.Errorf("median factory error: %v", err)
	}

	go slideWindow(wind, delays, medians)

	go writeResult(w, medians, errc)

	err = readFromFile(filepath, delays)
	if err != nil {
		return fmt.Errorf("median factory error: %v", err)
	}
	close(delays)

	err = <-errc
	return err
}

func slideWindow(w window.MedianInterface, delays <-chan int, medians chan<- float64) {
	for d := range delays {
		w.AddDelay(d)
		medians <- w.GetMedian()
	}
	close(medians)
}

func writeResult(w io.Writer, medians <-chan float64, errc chan<- error) {
	func() {
		for m := range medians {
			_, err := fmt.Fprintf(w, "%.1f\r\n", m)
			if err != nil {
				errc <- err
			}
		}
		errc <- nil
	}()
}

func readFromFile(filepath string, delays chan<- int) error {
	file, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		delay, err := strconv.Atoi(strings.TrimSpace(scanner.Text()))
		if err != nil {
			return err
		}

		delays <- delay
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}
