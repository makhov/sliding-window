package main

import (
	"flag"
	"github.com/makhov/sliding-window/pkg/app"
	"log"
	"os"
)

func main() {
	var (
		inFile  string
		outFile string
		alg     string
		size    int
		err     error
	)
	flag.StringVar(&inFile, "in", "", "path to input file")
	flag.StringVar(&outFile, "out", "", "path to output file, optional")
	flag.StringVar(&alg, "alg", "binary", "sliding window implementation algorithm. opts: array, binary.")
	flag.IntVar(&size, "size", 0, "sliding window size")

	flag.Parse()

	if inFile == "" {
		log.Fatal("file path is empty. see -h option for help")
	}

	if size == 0 {
		log.Fatal("window size is empty. see -h option for help")
	}

	w := os.Stdout
	if outFile != "" {
		w, err = os.Create(outFile)
		if err != nil {
			log.Fatalf("creating file %s errpr: %v", outFile, err)
		}
	}

	if err := app.Run(inFile, alg, size, w); err != nil {
		log.Fatal(err)
	}
}
