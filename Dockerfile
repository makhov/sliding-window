FROM golang:1.11 as build

COPY . /app
WORKDIR /app

RUN go build -o /app/bin/sw-cli ./cmd/cli


FROM scratch

COPY --from=build /app/bin/sw-cli /bin/sw-cli

ENTRYPOINT ["/bin/sw-cli"]