.PHONY: build
build:
	go build -o ./bin/sw-cli ./cmd/cli

.PHONY: test
test:
	go test ./... -short

.PHONY: test-full
test-full:
	go test ./...

.PHONY: bench
bench:
	go test ./pkg/window -bench . -benchtime 100ms



