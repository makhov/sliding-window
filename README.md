sliding-window
==============
Calculates the median in given dataset. Imple


Build
-----

```
$ make build 
```

Docker:
```
$ docker build -t sliding-window . 
```

Run
---
```
$ docker run --rm -v `pwd`:/app sliding-window -in /app/test2.csv -size 100
-1.0
256.0
200.0
241.0
282.0
…

$ docker run --rm -v `pwd`:/app sliding-window -in /app/test2.csv -size 100 -out /app/out.txt
$ cat `pwd`/out.txt
-1.0
256.0
200.0
241.0
282.0
…
```

Help
----
```
$ docker run --rm sliding-window -h
```

Tests
---------
```
$ make test # for short suite
$ make test-full # for full test suite
```

Benchmarks
---------
```
$ make bench
```

